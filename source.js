console.debug('PM Website Confirmation: Script successfully loaded');

let getImageLabel = () => {
  $('.image-label').each(function (index,el) {
    let dataID = $(this).attr('data-image-label');		
    $('.image-label > .label').eq(index).html(dataID);
  });
}

let automateURL_forPageFlow = () => {
  let currentURL 	= window.location.href,
      regex 		= new RegExp('/[^/]*$'),
      newURL 		= currentURL.replace(regex, '/');	

  $('.new-page-flow-element').each(function () {
    let slug = $('.page-flow-slug', this).html();
    $(this).attr('data-slug', slug);
    $('.page-flow-slug', this).remove();
  });

  $('.new-page-flow-element').on('click', function () {
    let slug = $(this).data('slug'),
        URL = newURL+''+slug;
    window.location.href = URL;
  });
}

let automateURL = (button, slug) => {
  let currentBtn = button,
      currentURL = window.origin;

  $(`${currentBtn}`).each(function () {
    let currentSlug = $(this).prev(`${slug}`).html();
    $(this).attr('data-slug', currentSlug);
    $(this).prev(`${slug}`).remove();
  }),

    $(`${currentBtn}`).on('click', function () {
    let slug 				= $(this).data('slug'),
        projectsURL 		= currentURL+''+'/projects/'+slug,
        servicesURL 		= currentURL+''+'/about-us/services/'+slug,
        currentButtonData = $(this).data('button');

    if ( currentButtonData == 'projects' ) {
      window.location.href = projectsURL;
    } else if ( currentButtonData == 'services' ) {
      window.location.href = servicesURL;
    }
  });
}

let automateURL_forSliders = () => {
  let buttonClass = '.btn.link-button',
      slugClass = '.explore-btn-slug';

  automateURL(buttonClass, slugClass);
}

let automateURL_subNavigation = () => { 
  let buttonClass = '.sub-navigation-item',
      slugClass = '.sub-nav-slug';

  automateURL(buttonClass, slugClass);
}

/** This is to make sure that whenever the user deletes
	the uploaded file. The data-value will be totally removed **/
let removeDataValueSetOnFileUploadField = () => {
  $('.w-icon-file-upload-remove').on('click', function() {
    $('input[type="file"]').removeData('value');
  });
}

let hideCareerApplicationForms__onSmallScreens = () => {
  if(window.innerWidth <= 1024) {
    $('#careers-application').remove();
    $('.application-form').remove();
    $('.application-form-modal').remove();
    $('.open-form-btn').remove();
  }
}

Webflow.push(function() {

  // Add Page label decorator automatically based on the title
  $('.page-label').html($(document).attr('title'));

  $('body').append('<div class="header-gradient"></div>');

  // Menu Interaction
  $('.menu-icon').click(function(e) {
    e.preventDefault();

    var menuVisibility = $('.nav-area').css('display');

    (menuVisibility == 'none') ?
      $('body').css('overflow', 'hidden'):
    $('body').css('overflow', 'auto'); 

    $('html').toggleClass('overflow-hidden');
  });

  // ----------------------------------------------------
  // Tab item

  $('.tab-item').on('click', function() {
    var $el = $(this);

    // Contain the selection of tab item to the
    // selection container and not all tab-item
    $el.parentsUntil('.panel-selection')
      .find('.tab-item')
      .removeClass('active');

    $el.addClass('active');
  });

  getImageLabel();
  automateURL_forPageFlow();
  automateURL_forSliders();
  automateURL_subNavigation();
  removeDataValueSetOnFileUploadField();
  hideCareerApplicationForms__onSmallScreens();
});



// ----------------------------------------------------
// Form Validation Functions

let validatePosition = (fieldID) => {

  let errorMessage = 'What position are you applying for?';

  if (!$(`#${fieldID}`).val()) {   
    if (!($(`#${fieldID}`).next('span').length)) {
      $(`#${fieldID}`).after(`<span class="error">${errorMessage}</span>`);
    } else {
      $(`#${fieldID} + span`).text(errorMessage);
    }

    $(`#${fieldID}`).addClass('hasError');
    return false;
  }

  if ($(`#${fieldID}`).val()) {
    $(`#${fieldID}`).removeClass('hasError');
    $(`#${fieldID}`).next('span').remove();
  }
}

let validateName = (fieldID) => {
  let letters 				 = /^[a-zA-Z ]*$/,
      fieldValue 			 = $(`#${fieldID}`).val(),
      errorMessage 			 = 'Please tell us your name!',
      errorMessageValidation = 'Please use regular letters only.';

  if (!$(`#${fieldID}`).val()) {
    if (!($(`#${fieldID}`).next('span').length)) {
      $(`#${fieldID}`).after(`<span class="error">${errorMessage}</span>`);
    } else {
      $(`#${fieldID} + span`).text(errorMessage);
    }
    $(`#${fieldID}`).addClass('hasError');
    return false;
  }

  if ($(`#${fieldID}`).val()) {
    if (!fieldValue.match(letters)) {
      if (!($(`#${fieldID}`).next('span').length)) {
        $(`#${fieldID}`).after(`<span class="error">${errorMessageValidation}</span>`);
      } else {
        $(`#${fieldID} + span`).text(errorMessageValidation);
      }

      $(`#${fieldID}`).addClass('hasError');
      return false;
    }

    if (fieldValue.match(letters)) {
      $(`#${fieldID}`).removeClass('hasError');
      $(`#${fieldID}`).next('span').remove();
    }
  }
}

let validateEmail = (fieldID) => {

  let isValidEmail 				= /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test($(`#${fieldID}`).val()),
      errorMessage 				= "Don't forget your email!",
      errorMessageValidation 	= 'Please use a valid email format.';

  if (!$(`#${fieldID}`).val()) {

    if (!($(`#${fieldID}`).next('span').length)) {
      $(`#${fieldID}`).after(`<span class="error">${errorMessage}</span>`);
    } else {
      $(`#${fieldID} + span`).text(errorMessage);
    }

    $(`#${fieldID}`).addClass('hasError'); 
    return false;
  } 

  if (!(isValidEmail)) {

    if (!($(`#${fieldID}`).next('span').length)) {
      $(`#${fieldID}`).after(`<span class="error">${errorMessageValidation}</span>`);
    } else {
      $(`#${fieldID} + span`).text(errorMessageValidation);
    }

    $(`#${fieldID}`).addClass('hasError'); 
    return false;

  } 

  if ((isValidEmail) && $(`#${fieldID}`).val()) {
    $(`#${fieldID}`).removeClass('hasError');
    $(`#${fieldID}`).next('span').remove();
  }
}

let validateMobile = (fieldID) => {
  let isValidMobileNumber 	= /^[+]?\d+$/.test($(`#${fieldID}`).val()),
      errorMessage 			= 'Please share your mobile number.',
      errorMessageValidation = 'Please use numbers only.';

  if (!$(`#${fieldID}`).val()) {
    if (!($(`#${fieldID}`).next('span').length)) {
      $(`#${fieldID}`).after(`<span class="error">${errorMessage}</span>`);
    } else {
      $(`#${fieldID} + span`).text(errorMessage);
    }

    $(`#${fieldID}`).addClass('hasError');
    return false;
  }

  if (!(isValidMobileNumber)) {
    if (!($(`#${fieldID}`).next('span').length)) {
      $(`#${fieldID}`).after(`<span class="error">${errorMessageValidation}</span>`);
    } else {
      $(`#${fieldID} + span`).text(errorMessageValidation);
    }

    $(`#${fieldID}`).addClass('hasError');
    return false;
  } 

  if ((isValidMobileNumber) && $(`#${fieldID}`).val()) {
    $(`#${fieldID}`).removeClass('hasError');
    $(`#${fieldID}`).next('span').remove();
  }
}

let validatePhone = (fieldID) => {

  let isValidPhoneNumber = /^[+]?\d+$/.test($(`#${fieldID}`).val()),
      errorMessage		 = 'Please use numbers only.';

  if (!(isValidPhoneNumber) && ($(`#${fieldID}`).val())) {
    if (!($(`#${fieldID}`).next('span').length)) {
      $(`#${fieldID}`).after(`<span class="error">${errorMessage}</span>`);
    } else {
      $(`#${fieldID} + span`).text(errorMessage);
    }
    $(`#${fieldID}`).addClass('hasError');
    return false;
  } 

  if ((!$(`#${fieldID}`).val()) || (isValidPhoneNumber)) {
    $(`#${fieldID}`).removeClass('hasError');
    $(`#${fieldID}`).next('span').remove();
  }
}

let validatePortfolioLink = (fieldID) => {

  let isValidLink 			 = /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi.test($(`#${fieldID}`).val()),
      errorMessage 			 = 'Please link your portfolio — this is important!',
      errorMessageValidation = 'Invalid portfolio link!';

  if (!$(`#${fieldID}`).val()) {

    if (!($(`#${fieldID}`).next('span').length)) {
      $(`#${fieldID}`).after(`<span class="error">${errorMessage}</span>`);
    } else {
      $(`#${fieldID} + span`).text(errorMessage);
    }

    $(`#${fieldID}`).addClass('hasError');
    return false;
  } 

  if (!(isValidLink)) {

    if (!($(`#${fieldID}`).next('span').length)) {
      $(`#${fieldID}`).after(`<span class="error">${errorMessageValidation}</span>`);
    } else {
      $(`#${fieldID} + span`).text(errorMessageValidation);
    }

    $(`#${fieldID}`).addClass('hasError');
    return false;

  }

  if ((isValidLink) && $(`#${fieldID}`).val()) {
    $(`#${fieldID}`).removeClass('hasError');
    $(`#${fieldID}`).next('span').remove();
  }
}

let validateMessage = (fieldID) => {

  let errorMessage = "Don't forget to answer this!";

  if (!$(`#${fieldID}`).val()) {

    if (!($(`#${fieldID}`).next('span').length)) {
      $(`#${fieldID}`).after(`<span class="error">${errorMessage}</span>`);
    } else {
      $(`#${fieldID} + span`).text(errorMessage);
    }

    $(`#${fieldID}`).addClass('hasError');
    return false;

  } 

  if ($(`#${fieldID}`).val()) {
    $(`#${fieldID}`).removeClass('hasError');
    $(`#${fieldID}`).next('span').remove();
  }
}

let validateResume = (fieldID, successStateID) => {

  let errorMessage 			= 'Please upload your resumé.';

  if ($(`#${fieldID}`).data('value') == undefined) {

    if (!($(`#${successStateID}`).next('span').length)) {
      $(`#${successStateID}`).after(`<span class="error">${errorMessage}</span>`);
    } else {
      $(`#${successStateID} + span`).text(errorMessage);
    }

    $(`#${fieldID}`).addClass('hasError');
    error = true;

  } 

  if ($(`#${fieldID}`).data('value') !== undefined) {
    $(`#${fieldID}`).removeClass('hasError');
    $(`#${successStateID}`).next('span').remove();
  }
}

  /*-------------- Scroll Marker --------------*/
let initScrollMarker = () => {
  let pageAnchors = $('[anchor-name]');
  let $scrollMarker = $('.scroll-marker-block');

  let injectScrollMarker = (anchorName, pointer) => {
      $scrollMarker.append(
          `<div class="scroll-marker" data-label="${anchorName}" data-pointer="pointer-${pointer}">
              <span class="label">${anchorName}</span>
          </div>`
      );
  };

  $scrollMarker.append()
  pageAnchors.each((a, el) => {
      let $anchor = $(el);
      let anchorName = $anchor.attr('anchor-name');

      $anchor.prepend(`<a class="_marker" name="pointer-${a}"></a>`);
      injectScrollMarker(anchorName, a);
  });

  $('.scroll-marker').on('click', (el) => {
      let $el         = $(el.currentTarget);
      let pointerName = $el.attr('data-pointer');
      let position    = $(`[name="${pointerName}"]`).offset().top;

      // Move to the position
      anime({
          targets  : $('html,body')[0],
          scrollTop: position,
          duration : 700,
          easing   : "easeInOutExpo"

      });

      $el.addClass('active');
  });


  let markers       = $('._marker');
  let scrollMarkers = $('.scroll-marker');
  let $window       = $(window);

  const TOP_TRESHOLD = -5;

  let highlightActivePosition = () => {
      // check the position of the markers relative to the scroll
      let scrollPosition = $window.scrollTop();
      let windowHeight   = $window.height();

      markers.each((a, el) => {
          let $el                   = $(el);
          let $container            = $el.parent();
          let containerHeight       = $container.height();
          let distanceFromTheTop    = scrollPosition - $el.offset().top;
          let distanceFromTheBottom = (distanceFromTheTop + containerHeight) - windowHeight;

          if (distanceFromTheTop >= TOP_TRESHOLD && distanceFromTheBottom <= windowHeight / 2) {
              scrollMarkers.removeClass('active');
              $(`[data-pointer="${$el.prop('name')}"]`).addClass('active');
          }
      });
  };

  $(document)[0].addEventListener("scroll", highlightActivePosition);

  highlightActivePosition();
}

initScrollMarker();

  
/*-------------- Best Browser Option --------------*/
let get_browser_version = () => {
  let ua = navigator.userAgent,tem,M=ua.match(/(opera|chrome|crios|edg|edgios|safari|firefox|fxios|msie|trident(?=\/))\/?\s*(\d+)/i) || []; 
  if( /trident/i.test(M[1]) ){
    tem=/\brv[ :]+(\d+)/g.exec(ua) || []; 
    return { name:'IE', version:(tem[1]||'')};
  }   
  if( (M[1] === 'Chrome') || (M[1] === 'CriOS') || (M[1] === 'FxiOS') || (M[1] === 'EdgiOS')){
    tem=ua.match(/\bOPR|Edg\/(\d+)/)
    if(tem!=null) { return { name:'Edge', version:tem[1]}; }
  }

  M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
  if((tem=ua.match(/version\/(\d+)/i))!=null) { M.splice(1,1,tem[1]); }
  return { name: M[0], version: M[1] };
}

let validateBrowserVersion = () => {
  let browser = get_browser_version(),
      smallDevice = window.innerWidth <= 1024;

  if (!smallDevice) { 
    if (
      ((browser.name == 'Chrome') && (browser.version < 83)) || 
      ((browser.name == 'Firefox') && (browser.version < 80)) ||
      ((browser.name == 'Safari') && (browser.version < 13)) ||
      ((browser.name == 'Edge') && (browser.version < 83))
    ) { 
      $('.current-version').html(`${browser.name} v.${browser.version}.`);
      $('.browser-option').removeClass('hidden');
    } else {
      $('.browser-option').remove();
    }

  } else {
    if (
      ((browser.name == 'Chrome') && (browser.version < 83)) ||
      ((browser.name == 'Edge') && (browser.version < 83)) ||
      ((browser.name == 'Firefox') && (browser.version < 79)) ||
      ((browser.name == 'Safari') && (browser.version < 9)) ||
      ((browser.name == 'CriOS') && (browser.version < 83)) ||
      ((browser.name == 'FxiOS') && (browser.version < 25))
    ) { 

      /** iOS check for Chrome and Firefox **/
      if (browser.name == 'CriOS') {
        browser.name = 'Chrome';
      } else if (browser.name == 'FxiOS') {
        browser.name = 'Firefox';
      } else if ((browser.name == 'EdgiOS')) {
        browser.name = 'Edge';
      }

      $('.current-version').html(`${browser.name} v.${browser.version}.`);
      $('.browser-option').removeClass('hidden');

    } else {   
      $('.browser-option').remove();
    }
  }
} 

let closeBrowserOption = () => {
  $('.close-btn.reco-browser').on('click', function () {
    $('.browser-option').hide();
  });
}

validateBrowserVersion();
closeBrowserOption();

/*-------------- Detecting Mobile OS --------------*/

let detectMobileOS = () => {
  let userOS,
      userOSver,
      ua = navigator.userAgent,
      uaindex,
      isSmallDevice = window.innerWidth <= 1024;

  if ( ua.match(/iPad/i) || ua.match(/iPhone/i) ){
    userOS = 'iOS';
    uaindex = ua.indexOf( 'OS ' );
  } else if ( ua.match(/Android/i) ) {
    userOS = 'Android';
    uaindex = ua.indexOf( 'Android ' );
  } else {
    userOS = 'unknown';
  }

  if ( userOS === 'iOS'  &&  uaindex > -1 ) {
    userOSver = ua.substr( uaindex + 3, 3 ).replace( '_', '.' );
  } else if ( userOS === 'Android'  &&  uaindex > -1 ){
    userOSver = ua.substr( uaindex + 8, 3 );
  } else {
    userOSver = 'unknown';
  }

  if (isSmallDevice) { 
    if ( (userOS === 'iOS') && (userOSver < 10)) { 
      $('.careers-videobg').hide();
    } else if ( (userOS === 'Android') && (userOSver < 5) ) {
      $('.careers-videobg').hide();
    }
  }
  // console.log(userOS + " " + userOSver);
}

detectMobileOS();


// Careers Page
let addPostionCardCount = () => {
  let count = 1,
      availablePosition = $('.available-position-item').length;

  $('.available-position-item').each(function () {
    $(this).find('.card-count').html('0' + count);
    count++;
  });
  $('.position-counter').html(availablePosition);
}

addPostionCardCount();

let charactersOnly = (fieldID) => {
  $(`#${fieldID}`).on('input keypress', function (e) {
    let regex =  /^[A-Za-zÀ-Üà-ü'.-\s\b]+$/i,
        isValid = regex.test(String.fromCharCode(!e.charCode ? e.which : e.charCode));

    if (!isValid) {
      e.preventDefault();
    }
  });
}

charactersOnly('op-applicant-name');
charactersOnly('c-applicant-name');

let numbersOnly = () => {
  $('input[type="tel"]').on('input keypress', function() {
    var key = event.keyCode || event.charCode || event.which;
    switch (key) {
      case 43:
      case 40:
      case 41:
        break;
      default:
        var regex = new RegExp("^[0-9.,/ $@()]+$");

        if ((key >= 48 && key <= 57) || key == 8 ||
            (key >= 35 && key <= 40) || key == 46) {
        } else {
          event.preventDefault();
          return false;
        }
        break;
    }
  });
}

numbersOnly();

let makeSelectFieldItemsDynamic = () => {
  let selectionItemCreatives 	= $('.selection-item--creatives'),
      selectionItemOperations = $('.selection-item--operations');

  selectionItemCreatives.each(function() {
    let item = $(this).text();
    $('#c-position > option:first-of-type').after(`<option value="${item}">${item}</option>`);
  }),

  selectionItemOperations.each(function() {
    let item = $(this).text();
    $('#op-position > option:first-of-type').after(`<option value="${item}">${item}</option>`);
  });
  
  if ($('.selection-item--creatives').length > 0) {
    $('.selection-items-collection--creatives').remove();
  } else {
    $('.selection-items-collection--creatives').show();
  }
  
  if ($('.selection-item--operations').length > 0) {
    $('.selection-items-collection--operations').remove();
  } else {
    $('.selection-items-collection--operations').show();
  }
  
  /**Disable Selection Field Placeholder**/
  $('.select-field option:nth-child(1)').prop('disabled', true);
}

makeSelectFieldItemsDynamic();

let selectFieldOnChange_valueColor = () => {
  $('.select-field').on('change', function () {
    $(this).addClass('active');
  });
}

selectFieldOnChange_valueColor();

let onFormOpen__OverflowHidden = () => {
  $('.open-form-btn').on('click', function () {
    $('body').addClass('overflow-hidden');
    $('html').addClass('overflow-hidden');
  });
}

onFormOpen__OverflowHidden();

let resetForm = () => {
  $('.application-form-modal').hide();
  $('span.error').remove();
  $('select').prop('selectedIndex', 0).removeClass('hasError');
  $(".select-field").removeClass('active');
  $('input').val('').removeClass('hasError');
  $('input[type="submit"]').val('Submit');
  $('textarea').val('').removeClass('hasError');
  $('#creatives-form').removeAttr('style');
  $('#operations-form').removeAttr('style');
  $('.w-form-done').removeAttr('style');
  $('.w-form-fail').removeAttr('style');
  $('.w-file-upload-uploading').removeAttr('style');
  $('.w-file-upload-success').removeAttr('style');
  $('.w-file-upload-error').removeAttr('style');
  $('.upload-resume-field').removeAttr('style');
  $('.w-file-upload-file-name').text('');
  $('.tab-menu').removeClass('hide');
  $('.tab-selection-label').removeClass('hide');
  $('.application-form-error').removeClass('visible');
  $('body').removeClass('overflow-hidden');
  $('html').removeClass('overflow-hidden');
  $('input[type="file"]').removeData('value');
  $('#tab-creatives').trigger('tap');
}

let resetFormOnTabChange = () => {
  $('span.error').remove();
  $('select').prop('selectedIndex', 0).removeClass('hasError');
  $(".select-field").removeClass('active');
  $('input').val('').removeClass('hasError');
  $('textarea').val('').removeClass('hasError');
  $('.w-form-done').removeAttr('style');
  $('.w-form-fail').removeAttr('style');
  $('.w-file-upload-uploading').removeAttr('style');
  $('.w-file-upload-success').removeAttr('style');
  $('.w-file-upload-error').removeAttr('style');
  $('.upload-resume-field').removeAttr('style');
  $('.w-file-upload-file-name').text('');
  $('.application-form-error').removeClass('visible');
  $('input[type="file"]').removeData('value');
  $('input[type="submit"]').val('Submit');
}

let onCareersTabsOnChange = () => {
  $('.w-tab-link').on('click', function () {
    resetFormOnTabChange();
  });
}

onCareersTabsOnChange();

let modalClose__Reset = () => { // ESC
  $( document ).on( 'keydown', function ( e ) {
    if ( e.keyCode === 27 ) { 
      resetForm(); 
    }
  });

  $('.close-button').on('click', function () {
    resetForm();
  });
}

modalClose__Reset();

let validateCreativesForm = () => {
  $('#creatives-form').submit( function (e) {
    let error,
        position						= 'c-position',
        name				 				= 'c-applicant-name',
        email 							= 'c-email-address',
        mobile 							= 'c-mobile',
        phone 							= 'c-phone',
        portfolio 					= 'c-portfolio-link',
        message 						= 'c-contribution-field',
        resume 							= 'Resume-CV',
        resumeSuccessID			= 'creatives-success-state',
        letters							= /^[a-zA-Z ]*$/,
        nameValue 					= $(`#${name}`).val(),
        isValidEmail 				= /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test($(`#${email}`).val()),
        isValidMobileNumber = /^\d+$/.test($(`#${mobile}`).val()),
        isValidPhoneNumber 	= /^\d+$/.test($(`#${phone}`).val()),
        isValidLink 				= /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi.test($(`#${portfolio}`).val());;

    if (!validatePosition(position)) { error = true; }

    if (!validateName(name)) { error = true; }

    if (!validateEmail(email)) { error = true; }

    if (!validateMobile(mobile)) { error = true; }

    if (!validatePhone(phone)) { error = true; }

    if (!validatePortfolioLink(portfolio)) { error = true; }

    if (!validateMessage(message)) { error = true; }

    if (!validateResume(resume, resumeSuccessID)) { error = true; }
    
    /** Focus on field that hasError **/    
    if (!($(`#${position}`).val())) {
      $(`#${position}`).focus();
    } else if (!($(`#${name}`).val()) || (!nameValue.match(letters))) {
      $(`#${name}`).focus();
    }  else if ((!$(`#${email}`).val()) || (!(isValidEmail))) {
      $(`#${email}`).focus();
    } else if ((!$(`#${mobile}`).val()) || (!(isValidMobileNumber))) {
      $(`#${mobile}`).focus();
    } else if (!(isValidPhoneNumber) && ($(`#${phone}`).val())) {
      $(`#${phone}`).focus();
    } else if ((!$(`#${portfolio}`).val()) || (!(isValidLink))) {
      $(`#${portfolio}`).focus();
    } else if (!$(`#${message}`).val()) {
      $(`#${message}`).focus();
    } else if (typeof $(`#${resume}`).data('value') == 'undefined') {
      $(`#${resume}`).focus();
    } else {
      error = false;
    }

    if (error) {
      return false;
    } else {
      $('.application-form-error').removeClass('visible');
      $('.tab-menu, .tab-selection-label').addClass('hide');
      return true;
    }
  });
}

validateCreativesForm();

let validateOperationsForm = () => {
  $('#operations-form').submit( function (e) {
    let error,
        position 						= 'op-position',
        name 								= 'op-applicant-name',
        email 							= 'op-email',
        mobile 							= 'op-mobile',
        phone 							= 'op-phone',
        message						 	= 'op-contribution-field',
        resume							= 'Resume-CV-2',
        resumeSuccessID			= 'operations-success-state',
        letters							= /^[a-zA-Z ]*$/,
        nameValue 					= $(`#${name}`).val(),
        isValidEmail 				= /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test($(`#${email}`).val()),
        isValidMobileNumber = /^\d+$/.test($(`#${mobile}`).val()),
        isValidPhoneNumber 	= /^\d+$/.test($(`#${phone}`).val());

    if (!validatePosition(position)) { error = true; }

    if (!validateName(name)) { error = true; }

    if (!validateEmail(email)) { error = true; }

    if (!validateMobile(mobile)) { error = true; }

    if (!validatePhone(phone)) { error = true; }

    if (!validateMessage(message)) { error = true; }

    if (!validateResume(resume, resumeSuccessID)) { error = true; }

    /** Focus on field that hasError **/    
    if (!($(`#${position}`).val())) {
      $(`#${position}`).focus();
    } else if (!($(`#${name}`).val()) || (!nameValue.match(letters))) {
      $(`#${name}`).focus();
    }  else if ((!$(`#${email}`).val()) || (!(isValidEmail))) {
      $(`#${email}`).focus();
    } else if ((!$(`#${mobile}`).val()) || (!(isValidMobileNumber))) {
      $(`#${mobile}`).focus();
    } else if (!(isValidPhoneNumber) && ($(`#${phone}`).val())) {
      $(`#${phone}`).focus();
    } else if (!$(`#${message}`).val()) {
      $(`#${message}`).focus();
    } else if ($(`#${resume}`).data('value') == undefined) {
      $(`#${resume}`).focus();
    } else {
      error = false;
    }

    if (error) {
      return false;
    } else {
      $('.application-form-error').removeClass('visible');
      $('.tab-menu, .tab-selection-label').addClass('hide');
      return true;
    }
  });
}

validateOperationsForm();

// Contact Us Page

charactersOnly('Name');

let validateContactUsForm = () => {
  $('#wf-form-Email-Form').submit( function (e) {
    let error,
        name 								= 'Name',
        email 							= 'Email',
        message							= 'Message',
        letters							= /^[a-zA-Z ]*$/,
        nameValue 					= $(`#${name}`).val(),
        isValidEmail 				= /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test($(`#${email}`).val());

    if (!validateName(name)) { error = true; }

    if (!validateEmail(email)) { error = true; }

    if (!validateMessage(message)) { error = true; }

    /** Focus on field that hasError **/    
    if (!($(`#${name}`).val()) || (!nameValue.match(letters))) {
      $(`#${name}`).focus();
      console.log('wat');
    } else if ((!$(`#${email}`).val()) || (!(isValidEmail))) {
      $(`#${email}`).focus();
    } else if (!$(`#${message}`).val()) {
      $(`#${message}`).focus();
    } else {
      error = false;
    }

    if (error) {   
      return false;
    } else {
      return true;
    }
  });
}

validateContactUsForm();

// Career Entry Page

charactersOnly('applicant-name');

let validateCareerEntryForm = () => {
  $('#careers-application').submit( function (e) {
    let error,
        name 								= 'applicant-name',
        email 							= 'applicant-email-address',
        mobile 							= 'applicant-mobile',
        phone								= 'applicant-phone',
        portfolio 					= 'applicant-porfolio',
        contrib							= 'applicant-contribution',
        resume							= 'Resume-CV',
        resumeSuccessID			= 'career-entry-success-state',
        letters							= /^[a-zA-Z ]*$/,
        nameValue 					= $(`#${name}`).val(),
        isValidEmail 				= /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test($(`#${email}`).val()),
        isValidMobileNumber = /^\d+$/.test($(`#${mobile}`).val()),
        isValidPhoneNumber 	= /^\d+$/.test($(`#${phone}`).val()),
        isValidLink 				= /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi.test($(`#${portfolio}`).val());;

    if (!validateName(name)) { error = true; }

    if (!validateEmail(email)) { error = true; }

    if (!validateMobile(mobile)) { error = true; }

    if (!validatePhone(phone)) { error = true; }

    if (!validatePortfolioLink(portfolio)) { error = true; }

    if (!validateMessage(contrib)) { error = true; }

    if (!validateResume(resume, resumeSuccessID)) { error = true; }

    /** Focus on field that hasError **/    
    if (!($(`#${name}`).val()) || (!nameValue.match(letters))) {
      $(`#${name}`).focus();
    }  else if ((!$(`#${email}`).val()) || (!(isValidEmail))) {
      $(`#${email}`).focus();
    } else if ((!$(`#${mobile}`).val()) || (!(isValidMobileNumber))) {
      $(`#${mobile}`).focus();
    } else if (!(isValidPhoneNumber) && ($(`#${phone}`).val())) {
      $(`#${phone}`).focus();
    } else if ((!$(`#${portfolio}`).val()) || (!(isValidLink))) {
      $(`#${portfolio}`).focus();
    } else if (!$(`#${contrib}`).val()) {
      $(`#${contrib}`).focus();
    } else if ($(`#${resume}`).data('value') == undefined) {
      $(`#${resume}`).focus();
    } else {
      error = false;
    }

    if (error) {   
      return false;
    } else {
      return true;
    }
  });
}

validateCareerEntryForm();

let specifyFormToShow = () => {
  if(window.innerWidth > 1024) {
    document.getElementById("career-entry-position").value = $('#career-entry-title').text();

    if ($('#career-type').html() == 'Creatives') {

    } else if ($('#career-type').html() == 'Operations') {
      var myobj = document.getElementById("portfolio-link-field");
      myobj.remove();
    }
  }
}

if ($('#careers-application').length) {
  specifyFormToShow();
}

let addClassToUL = () => {
  $('.career-entry-description > ul').prev('h3').addClass('career-description-title');
  $('.career-entry-description > h5').addClass('career-description-title sub-heading');
  $('h5').prev('h3').addClass('career-description-title no-list');
  $('h3').next('ul').addClass('career-entry-list');
  $('h5').next('ul').addClass('career-entry-sub-heading-list');

  $('<div class="separator"></div>').insertBefore(".career-description-title");
  $('.career-description-title.sub-heading').prev('div').remove();
  $('.career-entry-description > .separator:first-child').remove();
}

addClassToUL();

////////////////////////////////////////////////////////////////////////
// Slider Animation

let sliderContainer = $('.slider-panels-3d-plane');

let setupSlider = ($slider) => {
  let slides            = $("._3d-pane", $slider);
  let $root             = $slider.parent().parent();
  let $markersContainer = $root.find('.slider-marker');
  let $tabsContainer    = $root.find('.expertise-selection');

  let $leftHitArea  = $('> .slider-large-hit-area--left', $root);
  let $rightHitArea = $('> .slider-large-hit-area--right', $root);

  // Set the width of the slider container
  let $slidesContainer = $root.find('> .slider-panel-plane > .slider-panels-3d-plane');
  // Resize to the number of available elements
  $slidesContainer.width(slides.length * 100 + `vw`);

  ////////////////////////////////////////////////////////////////
  // Markers
  // Clean the marker first
  $markersContainer.find('.slider-mark').remove();

  // Inject the markers
  for (let a = 0; a < slides.length; a++) {
    $markersContainer.append(`<li data-slide="${a + 1}" class="slider-mark"></li>`);
  }

  $root.position = 0;

  let sliderObject = {
    root     : $root,
    container: $slidesContainer,
    markers  : $markersContainer,
    tabs     : $tabsContainer
  }

  // Bind Click on the marker
  $markersContainer.find('.slider-mark').on('click', (e) => {
    let $el = $(e.currentTarget);
    let position = $el.attr('data-slide');

    // Hide Left Hit Area when hitting first element
    if (position == 1) {
      $leftHitArea.hide();
      $rightHitArea.show();
    }
    // Hide Right Hit Area when hitting last element
    if (position == slides.length) { 
      $rightHitArea.hide();
      $leftHitArea.show();
    }

    // Show both when position is > 1 & < slides.length
    if ((position > 1) && (position < slides.length)) {
      $rightHitArea.show();
      $leftHitArea.show();
    } 

    goToSlide(sliderObject, position);
  });

  // Bind Click on the tab item
  $tabsContainer.find('.expertise-selection-item').on('click', (e) => {
    let $el = $(e.currentTarget);
    let position = $el.attr('data-slide');

    // Hide Left Hit Area when hitting first element
    if (position == 1) {
      $leftHitArea.hide();
      $rightHitArea.show();
    }
    // Hide Right Hit Area when hitting last element
    if (position == slides.length) { 
      $rightHitArea.hide();
      $leftHitArea.show();
    }

    // Show both when position is > 1 & < slides.length
    if ((position > 1) && (position < slides.length)) {
      $rightHitArea.show();
      $leftHitArea.show();
    } 

    goToSlide(sliderObject, position);
  });

  // Hide Left Hit Area on Page Load
  $leftHitArea.hide();

  $leftHitArea.on('click', (e) => {
    $rightHitArea.show();

    // Reached the first element
    if (sliderObject.root.position == 1) { 
      return; 
    } 

    // Hide Left Hit Area when hitting first element
    if (sliderObject.root.position == 2) {
      $leftHitArea.hide();
      $rightHitArea.show();
    }

    goToSlide(sliderObject, sliderObject.root.position - 1);
  });

  $rightHitArea.on('click', (e) => {
    //Show Left Hit Area on Page Load
    $leftHitArea.show();

    // Reached the last element
    if (sliderObject.root.position == slides.length) { 
      return; 
    }

    // Hide Right Hit Area when hitting last element
    if (sliderObject.root.position == slides.length-1) { 
      $rightHitArea.hide();
    }

    goToSlide(sliderObject, sliderObject.root.position + 1);
  });

  goToSlide(sliderObject, 1, $leftHitArea, $rightHitArea);
};

let goToSlide = (sliderObject, n) => {

  n = parseInt(n);

  let x            = (n - 1) * -100;
  let $markers     = $('.slider-mark', sliderObject.markers);
  let $sliderTitle = $('.slider-title-block', sliderObject.root);

  let $tabs = $('.expertise-selection-item > .tab-item', sliderObject.tabs);
  
  $('.slider-title', $sliderTitle).css({
    'margin-right': 0,
    'margin-left' : 0
  });


  // Set the current position
  sliderObject.root.position = n;

  // Move to position
  anime({
    targets   : sliderObject.container[0],
    keyframes: [
      {
        translateZ: -220,
        opacity   : .6,
        duration  : 400,
        easing    : 'easeInOutExpo'
      },
      {
        delay     : 50,
        translateX: `${x}vw`,
        duration  : 250,
        easing    : 'cubicBezier(0.570, 0.000, 0.000, 1.000);'
      },
      {
        translateZ: 0,
        duration  : 350,
        opacity   : 1,
        easing    : 'easeInOutExpo',
        delay     : 100
      }
    ]
  });


  // move the title cards as well
  // add some bit of delay
  anime({
    targets: $sliderTitle[0],
    keyframes: [
      {
        translateZ: 250,
        duration: 350,
        easing: 'easeInOutExpo',
        opacity: .8,
        delay: 50
      },
      {
        delay     : 320,
        translateX: `${x}vw`,
        duration  : 195,
        easing    : 'cubicBezier(0.570, 0.000, 0.000, 1.000);'
      },
      {
        translateZ: 0,
        opacity: 1,
        duration: 300,
        easing: 'easeInOutExpo',
        delay: 100
      },
    ]
  });

  let titles = [];
  $('.slider-title', $sliderTitle).each((i, el) => {
    titles.push(el);
  });

  let translateXvalPos;
  let translateXvalNega;
  
  let screenWidth = window.innerWidth;

  if (screenWidth > 491) {
    translateXvalPos = '43vw';
    translateXvalNega = '-43vw';
  } else {
    translateXvalPos = '0';
    translateXvalNega = '0';
  }

  anime({
    targets    : titles,
    translateX : 0,
    easing     : 'easeInOutExpo',
    duration   : 400,
    delay      : 200
  });

  anime({
    targets    : titles[n],
    translateX : translateXvalNega,
    easing     : 'easeInOutExpo',
    duration   : 550,
    delay      : 800
  });

  anime({
    targets    : titles[n - 2],
    translateX: translateXvalPos,
    easing     : 'easeInOutExpo',
    duration   : 550,
    delay      : 800
  });

  // Mark the marker as active
  $markers.removeClass('active');
  $($markers[n - 1]).addClass('active');

  // Mark the marker as active
  $tabs.removeClass('active');
  $($tabs[n - 1]).addClass('active');
};


sliderContainer.each((i, slider) => {
  let $slider = $(slider);
  setupSlider($slider); 
});